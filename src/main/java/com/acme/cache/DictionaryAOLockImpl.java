package com.acme.cache;


import rx.Observable;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;


/**
 * @author Арамис
 */
public class DictionaryAOLockImpl extends DictionaryAOBase
{
    private volatile Map<String, Integer> dictionary;

    private final Lock lock = new ReentrantLock();


    public DictionaryAOLockImpl(
            Supplier<Map<String, Integer>> dictionaryLoader,
            Observable<String> invalidationMessageSource)
    {
        super(dictionaryLoader, invalidationMessageSource);
    }


    @Override
    public Integer getSetting(String keyName)
    {
        var localDictionary = this.dictionary;
        if (localDictionary != null)
        {
            return localDictionary.get(keyName);
        }

        lock.lock();
        try
        {
            localDictionary = this.dictionary;
            if (localDictionary != null)
            {
                return localDictionary.get(keyName);
            }

            localDictionary = dictionaryLoader.get();
            dictionary = localDictionary;
            return localDictionary.get(keyName);
        }
        finally
        {
            lock.unlock();
        }

    }


    @Override
    protected void invalidateInternal()
    {
        dictionary = null;
    }
}
