package com.acme.cache;


/**
 * Интерфейс бизнес-справочника
 */
public interface DictionaryAO
{
    /**
     * Получение значения целочисленной настройки по строковому ключу.
     * <p>
     * Метод будет вызываться несколькими потоками одновременно по одному или разным ключам.
     *
     * @param keyName Имя ключа
     * @return Значение настройки, возможно {@literal null}
     */
    Integer getSetting(String keyName);
}
