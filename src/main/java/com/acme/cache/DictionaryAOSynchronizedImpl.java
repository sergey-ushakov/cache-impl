package com.acme.cache;


import rx.Observable;

import java.util.Map;
import java.util.function.Supplier;


/**
 * @author Портос
 */
public class DictionaryAOSynchronizedImpl extends DictionaryAOBase
{
    private Map<String, Integer> dictionary;


    public DictionaryAOSynchronizedImpl(
            Supplier<Map<String, Integer>> dictionaryLoader,
            Observable<String> invalidationMessageSource)
    {
        super(dictionaryLoader, invalidationMessageSource);
    }


    @Override
    public synchronized Integer getSetting(String keyName)
    {
        if (dictionary == null)
        {
            dictionary = dictionaryLoader.get();
        }
        return dictionary.get(keyName);
    }


    @Override
    protected synchronized void invalidateInternal()
    {
        dictionary = null;
    }
}
