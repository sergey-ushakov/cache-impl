package com.acme.cache;


import com.google.common.base.Suppliers;
import rx.Observable;

import java.util.Map;
import java.util.function.Supplier;


/**
 * @author Атос
 */
public class DictionaryAOGuavaImpl extends DictionaryAOBase
{
    private volatile Supplier<Map<String, Integer>> cachedSupplier;


    public DictionaryAOGuavaImpl(
            Supplier<Map<String, Integer>> dictionaryLoader,
            Observable<String> invalidationMessageSource)
    {
        super(dictionaryLoader, invalidationMessageSource);
        resetCache();
    }


    @Override
    public Integer getSetting(String keyName)
    {
        return cachedSupplier.get().get(keyName);
    }


    @Override
    protected void invalidateInternal()
    {
        resetCache();
    }


    private void resetCache()
    {
        cachedSupplier = Suppliers.memoize(dictionaryLoader::get);
    }
}
