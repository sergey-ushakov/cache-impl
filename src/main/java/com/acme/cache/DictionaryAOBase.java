package com.acme.cache;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Notification;
import rx.Observable;
import rx.Subscription;
import rx.internal.schedulers.ExecutorScheduler;
import rx.internal.util.RxThreadFactory;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;


/**
 * Базовый класс, обеспечивающий интеграцию кеша в контекст приложения.
 * <p>
 * Все реализации кеша должны наследоваться от этого класса.
 */
public abstract class DictionaryAOBase implements DictionaryAO
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Доступ к внешнему загрузчику справочника.
     */
    protected final Supplier<Map<String, Integer>> dictionaryLoader;

    /**
     * Источник загрузок
     */
    protected final Observable<String> invalidationMessageSource;

    protected final RxThreadFactory threadFactory;

    protected final ExecutorService executor;

    protected final Subscription subscription;


    public DictionaryAOBase(
            Supplier<Map<String, Integer>> dictionaryLoader,
            Observable<String> invalidationMessageSource)
    {
        this.dictionaryLoader = dictionaryLoader;
        this.invalidationMessageSource = invalidationMessageSource;

        threadFactory = new RxThreadFactory("ChangesThread");
        executor = Executors.newSingleThreadExecutor(threadFactory);
        subscription = invalidationMessageSource
                .observeOn(new ExecutorScheduler(executor))
                .doOnEach(this::invalidate)
                .subscribe();
    }


    private void invalidate(Notification<? super String> notification)
    {
        logger.info("Got invalidation with reason {}", notification.getValue());
        try
        {
            invalidateInternal();
        }
        catch (Exception e)
        {
            logger.warn("Error occurred while cache invalidation", e);
        }
    }


    protected abstract void invalidateInternal();

    @Override
    public abstract Integer getSetting(String keyName);
}
